# 3. GitLab
Start collaborating!
In this chapter you are going to learn what is git and gitlab and the diferrences, Why you should learn git and how to use it.

## Introduction to Git

What is Git?
Git is the technology to help us to caliborate with people.

What is Github?
Github is a webstie and an application that works with git.

Why should you care?
If you would like to work with people in a team. This technology is useful and not only limited in writing a code.

Novel Writing Anology
Using git to help writing a novel. Imagine that you are a writer and you have some huge story in your head. Before you will the essay like Essay_v1.txt, Essay_v2 and Essay_Final. Furthermore, you would like to edit the version 2 and save it into Essay_final_final. You would reliase how anoying and terrible it is. This is why git is useful. Git help to go back to the last point and it is not limited to one file. It saves as a check point evertime you would save it to respository.

## Log in
[GitLab sign in](https://gitlab.com/users/sign_in)
![](../images/gitLab/signIn.PNG)

| Chinese   Name | User Name   |
|----------------|-------------|
| 夏一帆          | Xia.YiFan   |
| 陈戈睿          | Chen.GeRui  |
| 农宇璠          | Nong.YuFan  |

Fill your user name and password and click sign in.


## GitLab
After you are successfully log in into your account, you can see your project and click the link. 
```
fablabO / 2020 / labs / Nice / Students / your name.
```
![](../images/gitLab/project.PNG)

This is the what you get inside your project folder and open the docs file().
![](../images/gitLab/projectFolder.PNG)

## Edit document
Open the file that you want to edit. 
Index is the file that automatically loads when a web browser starts
![](../images/gitLab/openIndex.PNG)

Edit your file here.
![](../images/gitLab/editFile.PNG)

After you are done editing your file, click commit.
Please tell me about yourself on this page.
## Useful links

- [GitLab web editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html)

