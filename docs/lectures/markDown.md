# 2. Markdown
This is intended as a quick reference and showcase. For more complete info, see [GitHub Pages](https://help.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax#links).

- Headers
- Lists
- Links
- Images
- Code



# Headings
```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:
Alt-H1
======

Alt-H2
------

```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:
Alt-H1
======

Alt-H2
------

# Lists
```
1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses

```

1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses


# Links

There are two ways to create links.

```
[go to google](https://www.google.com)


```

[go to google](https://www.google.com)



# Image

```
![](../images/markDown/markDown.png)
```

![](../images/markDown/markDown.png)


# Code

Inline `code` has `back-ticks around` it.


```
Inline `code` has `back-ticks around` it.

```

Blocks of code are either fenced by lines with three back-ticks <code>```</code>




# reference

[adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists)
[stackedit](https://stackedit.io/app#)
