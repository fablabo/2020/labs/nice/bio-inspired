# 3. Git
Start collaborating!
In this chapter you are going to learn what is git and gitlab and the diferrences, Why you should learn git and how to use it.

## Introduction to Git

What is Git?
Git is the technology to help us to caliborate with people.

What is Github?
Github is a webstie and an application that works with git.

Why should you care?
If you would like to work with people in a team. This technology is useful and not only limited in writing a code.

Novel Writing Anology
Using git to help writing a novel. Imagine that you are a writer and you have some huge story in your head. Before you will the essay like Essay_v1.txt, Essay_v2 and Essay_Final. Furthermore, you would like to edit the version 2 and save it into Essay_final_final. You would reliase how anoying and terrible it is. This is why git is useful. Git help to go back to the last point and it is not limited to one file. It saves as a check point evertime you would save it to respository.

## Installation
Please download and install the following software

- Git

Windows : [Git](https://git-scm.com/downloads)

Mac OS  : [Git scm](https://git-scm.com/downloads)

Linux   : ```apt-get install git ```

![](/images/git/git.png)


Note    : Make sure to select the option “Use git from Git Bash only” during the installation, leave the other option as default.

![](../images/git/gitBash.png)

- Brackets
Download and install :  [Brackets](http://brackets.io/)

![](../images/git/brackets.png)


## GitLab Account
Please sign up and sign in for a GitLab account. [Registration for GitLab](https://gitlab.com/users/sign_up)

![](../images/git/gitLabAccount.png)

Take note of your GitLab user name and tell it to the admin, wait until the admin add your repository and give the permission.

## Git Setup
Adding an SSH key to your Gitlab account

- User name :
```git config --global user.name "put your user name here"```

- Email     :
```git config --global user.email "put your email here ```

- Generate SSH key
```
ssh-keygen -t rsa -C "your mail address"
```

- Copy your SSH key to the clipboard by using one of the commands below depending on your Operating System:

- Git Bash on Windows:
```cat ~/.ssh/id_rsa.pub | clip```

- macOS:
```pbcopy < ~/.ssh/id_rsa.pub```

- WSL/ GNU/ Linux (requires the xclip package):
```Xclip -sel clip < ~/.ssh/id_rsa.pub```

You can also open the key in a graphical editor and copy it from there, but not to accidentally change anything.

![](../images/git/gitSetup.png)

- Add the SSH key to your gitlab account.[Key](https://gitlab.com/profile/keys)

![](../images/git/setupSSH.png)

Paste your public SSH key, which is usually contained in the file '~/.ssh/id_ed25519.pub' or '~/.ssh/id_rsa.pub' and begins with 'ssh-ed25519' or 'ssh-rsa'. Don't use your private SSH key.

![](../images/git/SSHKey.png)

## Clone Repository
Clone your repository to create a working copy of a local repository
[projects](https://gitlab.com/dashboard/projects)

![](../images/git/project.png)

Copy and paste the link with SSH or HTTPS:
Create a working copy of a local repository by running the command
```git clone “paste the url here”```

![](../images/git/clone.png)

Cloning from Gitlab can be done using SSH which doesn’t require you to enter your username or password but will mean you need to setup SSH keys or through HTTPS where you will need your username and password to be specified for each push and pull.

![](../images/git/gitClone.png)

# Edit your website
Let’s try to add some file to our local repository ( use the HTML files you have created before using Brackets):
Open your local repository, eg: Open C:\Users\fabla\helloworld and open the file as Brackets project.

![](../images/git/openAsBrackets.png)

Edit the index.html file using Brackets or any text editor.

![](../images/git/bracketsVeiwer.png)

Make sure to save it afterwards.
Open git bash or terminal and change your directory using “cd” on the command line
```cd “name of the directory”```

![](../images/git/cd.png)

# Git Workflow
Git add and commit
You can propose changes using

```git add *```

or

```git <filename>```

This is the first step in the basic workflow. To actually commit these changes use

```git commit -m "Commit massage"```

![](../images/git/gitAdd.png)

Git push
Your changes are now in the HEAD of your local working copy. To send those changes to your remote repository, execute

```git push```
or
```git push origin master```

![](../images/git/gitPush.png)

Now you can free to publish, modify and re-publish the page several times using git workflow. 


## Admin Task
-	Add project member
Open the project page in GitLab, then click on Settings and select Members.
Type the name of the User you want to add to your project into the Select members to invite field.
Select a role for the user. Generally, you should give a fellow developer at least Developer Level Access.

-	Open the user project page and click settings, select protected branches and click the “unprotect” to completely remove protection from branch.


## Useful links

- [GitLab docs](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account)

